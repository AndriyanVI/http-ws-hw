import { updateGameStartTimer, updateGameTimer, updateEnteredText } from "../helpers/viewHelper.mjs";
import { createElement, addClass } from "../helpers/domHelper.mjs";

export const gameStartTimer = (timeToGameStart, startGame) => {
  let currentTime = timeToGameStart;

  let timerId = setInterval(() => {
    updateGameStartTimer(currentTime);
    if (currentTime == 0) {
      clearInterval(timerId);
      startGame();
    }
    currentTime--;
  }, 1000);
};

export const getText = async textId => {
  const response = await fetch(`/game/texts/${textId}`);
  return await response.text();
};

export const gameTimer = timeToGameEnd => {
  let currentTime = timeToGameEnd;

  let timerId = setInterval(() => {
    updateGameTimer(currentTime);
    if (currentTime == 0) {
      clearInterval(timerId);
    }
    currentTime--;
  }, 1000);
};

export const updateGameText = (text, currentIndex, userChar) => {
  const currentTextChar = text.charAt(currentIndex);
  if (currentTextChar === userChar) {
    const enteredText = text.slice(0, currentIndex);
    const correctTextContainer = createElement({ tagName: "span" });
    const updatedText = enteredText + userChar;
    correctTextContainer.innerText = updatedText;
    const notEnteredText = text.slice(currentIndex + 1);

    if (notEnteredText.length) {
      const notEneteredTextContainer = createElement({ tagName: "span" });
      notEneteredTextContainer.innerText = notEnteredText;
      addClass(correctTextContainer, "text__correct");
      updateEnteredText([correctTextContainer, notEneteredTextContainer]);
    } else {
      addClass(correctTextContainer, "text__correct--full");
      updateEnteredText([correctTextContainer]);
    }
    return true;
  }
  return false;
};

export const updateUserProgress = (text, currentIndex) => {
  return Math.round(100 / (text.length / currentIndex));
};
