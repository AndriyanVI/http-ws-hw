export const createNewUserRoom = username => {
  return { username, isReady: false, progress: 0 };
};

export const createNewRoom = (roomName, username) => {
  const user = createNewUserRoom(username);
  const newRoom = { roomName, users: [user], isGameStart: false };
  return newRoom;
};

export const checkIsRoomFull = (room, maxUsers) => {
  const isRoomFull = room.users.length >= maxUsers;
  return isRoomFull;
};

export const checkIsGameStart = room => room.isGameStart;

export const checkRoomAvailability = (room, maxUsers) => {
  const isRoomFull = checkIsRoomFull(room, maxUsers);
  const isGameStart = checkIsGameStart(room);
  if (isRoomFull || isGameStart) {
    const error = "Game is started or room is full. Join to another room or create new room.";
    return { isRoomAvailable: false, error };
  }
  return { isRoomAvailable: true, error: null };
};

export const addNewUserToRoom = (room, user) => {
  const users = [...room.users, user];
  const updatedRoom = { ...room, users };
  return updatedRoom;
};

export const removeUserFromRoom = (room, username) => {
  const users = room.users.filter(user => user.username !== username);
  const updatedRoom = { ...room, users };
  return updatedRoom;
};

export const checkIsRoomEmpty = room => room.users.length;

export const toDefaultRoom = room => {
  const updatedUsers = room.users.map(user => {
    return { ...user, isReady: false, progress: 0 };
  });
  return { ...room, isGameStart: false, users: updatedUsers };
};
