export const normalizeString = str => str.trim().toLowerCase();

export const getItemIndex = (items, itemName, prop) => items.findIndex(item => item[prop] === itemName);

export const checkIsItemExist = (items, itemName, prop) => {
  const normalizeItemName = normalizeString(itemName);
  const itemIndex = getItemIndex(items, normalizeItemName, prop);
  const isItemExists = itemIndex !== -1;
  return isItemExists;
};
