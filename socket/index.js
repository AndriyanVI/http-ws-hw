import { addUser, deleteUser } from "./services/userService";
import { displayRoomsForUser, addRoom, updateRooms, updateLeavingRooms, getRoom } from "./services/roomsService";
import { createRoom, joinRoom, leaveRoom } from "./services/roomService";
import { changeReadyStatus, updateGameProgress, endGame } from "./services/gameService";

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    const userId = socket.id;
    const { error } = addUser(username, userId);
    if (error) {
      socket.emit("CONNECTION_ERROR", error);
    }
    displayRoomsForUser(socket);
    socket.on("CREATE_ROOM", roomName => {
      const createdRoom = createRoom(roomName, username, socket);
      addRoom(createdRoom, io);
    });
    socket.on("JOIN_ROOM", roomName => {
      const updatedRoom = joinRoom(roomName, username, socket);
      updateRooms(updatedRoom, io);
    });
    socket.on("LEAVE_ROOM", roomName => {
      const updatedRoom = leaveRoom(roomName, username, socket);
      updateRooms(updatedRoom, io);
    });
    socket.on("CHANGE_READY_STATUS", roomName => {
      const updatedRoom = changeReadyStatus(roomName, username, socket, io);
      updateRooms(updatedRoom, io);
    });
    socket.on("UPDATE_GAME_PROGRESS", ({ roomName, updatedProgress }) => {
      const updatedRoom = updateGameProgress(roomName, username, updatedProgress, io);
      updateRooms(updatedRoom, io);
    });
    socket.on("GAME_TIME_ENDED", roomName => {
      const room = getRoom(roomName);
      const updatedRoom = endGame(room, io);
      updateRooms(updatedRoom, io);
    });
    socket.on("disconnect", () => {
      deleteUser(userId);
      updateLeavingRooms(username, socket, io);
    });
  });
};
