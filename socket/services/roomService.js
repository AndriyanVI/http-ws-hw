import {
  createNewRoom,
  createNewUserRoom,
  checkRoomAvailability,
  addNewUserToRoom,
  removeUserFromRoom
} from "../helpers/roomHelper";
import { checkIsRoomExist, getRoom } from "./roomsService";
import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../config";

export const createRoom = (roomName, username, socket) => {
  const isRoomExist = checkIsRoomExist(roomName);
  if (isRoomExist) {
    const error = `Room with name "${roomName}" already exist. Create room with new name or join to another room.`;
    socket.emit("CREATE_ROOM_ERROR", error);
    return { isRoomCreated: false, room: null };
  } else {
    socket.join(roomName);
    const newRoom = createNewRoom(roomName, username);
    socket.emit("JOINED_TO_ROOM", newRoom);
    return { isRoomCreated: true, newRoom };
  }
};

export const joinRoom = (roomName, username, socket) => {
  const room = getRoom(roomName);
  const { isRoomAvailable, error } = checkRoomAvailability(room, MAXIMUM_USERS_FOR_ONE_ROOM);
  if (!isRoomAvailable) {
    socket.emit("JOIN_ROOM_ERROR", error);
    return { isRoomUpdated: false, room: null };
  } else {
    const newUserRoom = createNewUserRoom(username);
    const updatedRoom = addNewUserToRoom(room, newUserRoom);
    socket.join(updatedRoom.roomName);
    socket.emit("JOINED_TO_ROOM", updatedRoom);
    socket.to(roomName).emit("UPDATE_USERS", updatedRoom);
    return { isRoomUpdated: true, room: updatedRoom };
  }
};

export const leaveRoom = (roomName, username, socket) => {
  const room = getRoom(roomName);
  const updatedRoom = removeUserFromRoom(room, username);
  socket.leave(room);
  socket.emit("LEAVE_FROM_ROOM");
  const isRoomEmpty = !room.users.length;
  if (!isRoomEmpty) {
    socket.to(roomName).emit("UPDATE_USERS", updatedRoom);
  }
  return { isRoomUpdated: true, room: updatedRoom };
};
