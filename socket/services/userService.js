let users = [];

export const addUser = (username, id) => {
  username = username.trim().toLowerCase();
  const isUserExists = users.findIndex(user => user.username === username);
  if (isUserExists !== -1) {
    return { error: `User with username ${username} already exist.` };
  }
  const newUser = { username, id };
  users.push(newUser);
  return { error: null, users };
};

export const deleteUser = id => {
  users = users.filter(user => user.id !== id);
  return users;
};
